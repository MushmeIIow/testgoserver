package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"sync"
	"time"
)

const (
	maxOrders = 50

	orderIdxBits = 6
	orderIdxMask = 1<<orderIdxBits - 1
	orderIdxMax  = 63 / orderIdxBits
	orderBytes   = "abcdefghijklmnopqrstuvwxyz"
	orderIDLen   = 2
)

var tick = time.Tick(200 * time.Millisecond)
var requests [maxOrders]string
var requestCount counterRequests

type counterRequests struct {
	sync.RWMutex
	Counter map[string]int
}

func generateRandomID() string {
	b := make([]byte, orderIDLen)
	for i, cache, remain := orderIDLen-1, rand.Int63(), orderIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = rand.Int63(), orderIDLen
		}
		if idx := int(cache & orderIdxMask); idx < len(orderBytes) {
			b[i] = orderBytes[idx]
			i--
		}
		cache >>= orderIdxBits
		remain--
	}
	return string(b)
}

func getRandomOrder() string {
	request := requests[rand.Intn(maxOrders)]
	requestCount.Lock()
	defer requestCount.Unlock()
	requestCount.Counter[request]++
	return request
}

func allRequests(w http.ResponseWriter, r *http.Request) {
	requestCount.RLock()
	defer requestCount.RUnlock()
	for k, v := range requestCount.Counter {
		fmt.Fprintln(w, k, " - ", v)
	}
}

func request(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, getRandomOrder())
}

func changeOrder() {
	for {
		select {
		case <-tick:
			requests[rand.Intn(maxOrders)] = generateRandomID()
		}
	}
}

func main() {
	rand.Seed(time.Now().UnixNano())
	requestCount = counterRequests{Counter: make(map[string]int)}

	for i := range requests {
		requests[i] = generateRandomID()
	}
	go changeOrder()
	http.HandleFunc("/request", request)
	http.HandleFunc("/admin/requests", allRequests)
	http.ListenAndServe(":8080", nil)
}
